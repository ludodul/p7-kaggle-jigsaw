### Mean of Ridge
 * Score original 0.858
 * Analyse du texte inexistante
 * Text cleaning très succin

### Tentatives d'amélioration
 * Meilleur text cleaning en ajoutant stemmer et lemmatizer 0.771
 * Tentative d'optimisation de l'hyperparamètre alpha 0.855
 * Tentative  en améliorant les poids des différentes catégories 0.834

### Conclusion
 * Améliorer ce score ne semble pas facile, et la stratégie de la moyenne semble être efficace pour lisser les scores.
 * Il semble y avoir un plafond de verre sur le jeu de validation à hauteur de 68%. Et ce n'est pas nécessairement le meilleur modèle sur le jeu de validation qui est le meilleur sur le jeu à soumettre.
 

