### Stratégie One Vs All
On applique ici une classification One Vs All
lien: https://www.kaggle.com/tisonludovic/ovr-jigsaw-sgd

 * Sélection de l'algorithme SGD Classififier qui est ici le plus performant pour F1 score 0.806, recall 0.81 et precision 0.817
 * sans poids, juste la somme de la fonction de décision : 0.768
 * prédiction avec les poids "classiques"  :0.639
 * fonction de décision avec les poids classiques : 0.81
 * Sans le stemmer : 0.764

### Conclusions:
 * Le meilleur résultats est obtenu avec les poids appliqués à la fonction de décision, et avec le stemmer Lancaster
 * Utilisation du stemmer dans d'autres stratégies?
 * Les résultats restent moins bons que les méthodes classiques de régression.