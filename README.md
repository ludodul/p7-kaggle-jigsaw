### Projet sur la compétition kaggle Jigsaw

Lien vers la compétition [ici](https://www.kaggle.com/c/jigsaw-toxic-severity-rating/overview])

Il s'agit d'une compétition sur le scoring de toxicité d'un commentaire.  
Nous allons ici analyser du texte via des algorithmes de Machin Learning afin d'en tirer la toxicité.

## Préparation de l'environnement de travail
### Installation de l'environnement virtuel

```
python -m venv env
```
### Activation de l'environnement virtuel
Linux :
```
source env/bin/activate
```

Windows:
```
.\env\Scripts\activate
```
### Installation des dépendances

```
pip install -r pip-requirements.txt
```
### Désactivation de l'environnement virtuel
```
deactivate
```
