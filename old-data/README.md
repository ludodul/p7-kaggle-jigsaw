### Stratégie olddata + adjusting

 * On charge un ancien dataset pour augmenter le nombre de données toxiques
 * On ajuste le score avec un score précédent.

### Conclusions
 * Amélioration en faisant la moyenne de 3 modèles Ridge 0.5, 1 et 3  : 0.866
 * L'ajout du stemmer Lancaster fait descendre très fortement le score : 0.813
 * tentative avec la moyenne de 8 modèles : 0.863